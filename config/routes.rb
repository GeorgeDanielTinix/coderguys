Rails.application.routes.draw do
 
  # POST /urls
  post '/urls', to: 'urls#create', as: :shorten
  
  # GET /urls/1
  get   '/urls/:id', to: 'urls#show', as: :show

  # GET /:code
  get  '/:code', to: 'urls#redirection', as: :redirect
  
  # GET /:code/stats
  get  '/:code/stats', to: 'urls#stats', as: :stats

end
