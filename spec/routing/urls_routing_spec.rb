require 'rails_helper'

RSpec.describe UrlsController, type: :routing do
  describe "routing" do

    it "routes /urls to #create" do
      expect(post: "/urls").to route_to("urls#create")
    end

    it "routes to #show" do
      expect(get: "/urls/1").to route_to("urls#show", id: "1")
    end

    it " routes to #redirection" do
      expect(get: "/code").to route_to("urls#redirection", code: "code")
    end

    it "routes to #stats" do
      expect(get: "/code/stats").to route_to("urls#stats", code: 'code')
    end
  end
end

