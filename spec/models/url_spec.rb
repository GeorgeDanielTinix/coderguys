require 'rails_helper'

RSpec.describe Url, type: :model do

  it "is invalid without a url" do
    url = Url.new(url: nil)
    url.valid?
    expect(url.errors[:url]).to include("can't be blank")
  end

  it "validates format regexp url" do
    url = Url.new(url: "@@@testing_asdf##")
    url.valid?
    expect(url.errors[:url]).to include("You provided invalid URL")
  end

  it 'is invalid with a duplicate url.code address' do
    Url.create(
      url: "http://exampl.com",
      code: "test123",
      last_usage: Time.now.iso8601,
      usage_count: "1"
    )
     url = Url.create(
       url: "http://example.com",
      code: "test123",
      last_usage: Time.now.iso8601,
      usage_count: "1"
    )
    url.valid?
    expect(url.errors[:code]).to include('has already been taken')
  end 
end
