class UrlsController < ApplicationController
  before_action :find_url, only: %i[stats redirection]

  # POST /urls
  def create
    @url = Url.new(url_params)
    
    if @url.save
      render json: { code: @url.code }, status: :created
    else
      render json: @url.errors, status: :unprocessable_entity
    end
  end

   # GET /urls/1
  def show 
    @url = Url.find(params[:id])
    render json: @url
  end

  # GET /:code/stats
  def stats
    render json: {created_at: @url.created_at, last_usage: @url.last_usage, usage_count: @url.usage_count}
  end

  # It's a redirect response including the target URL in its `Location` header.
  # GET /:code
  def redirection
    if @url.currently_usage
      render status: :found, location: @url.url
    else
      render status: :internal_error
    end
  end

  private

  def find_url
    @url = Url.find_by_code(params[:code])
    render status: :not_found unless @url
  end

  def url_params
    params.require(:url).permit(:url, :code)
  end
end
