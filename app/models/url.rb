class Url < ApplicationRecord

  # Validations
  
  VALID_URL_REGEXP = /\A(?:(?:http|https):\/\/)?([-a-zA-Z0-9.]{2,256}\.[a-z]{2,4})\b(?:\/[-a-zA-Z0-9@,!:%_\+.~#?&\/\/=]*)?\z/

  validates :code, uniqueness: true
  validates :url, presence: true, on: :create,
    format: { with: VALID_URL_REGEXP, message: 'You provided invalid URL' }

  before_create :generate_short_code

  def generate_short_code
    self.code = SecureRandom.hex(3) if self.code.nil?
  end

  def currently_usage
    self.update(usage_count: self.usage_count + 1, last_usage: Time.now.iso8601)
  end
end
